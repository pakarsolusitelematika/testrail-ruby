
def param_stringify(opts)
  opts.to_a.map { |x| "#{x[0]}=#{x[1]}" }.join("&")
end
