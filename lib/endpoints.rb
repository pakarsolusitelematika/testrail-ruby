module Endpoints
##########################++
####get_case################
############################
  # Returns an existing test case
  # @param case_id [int] The id of the test case you want.
  # @example Code Example
  #   @client.get_case(1)
  # @example Endpoint Example
  #   GET index.php?/api/v2/get_case/1
  # @example Response Example
  #    {
  #      "created_by": 5,s
  #      "created_on": 1392300984,
  #      "custom_expected": "..",
  #      "custom_preconds": "..",
  #      "custom_steps": "..",
  #      "custom_steps_separated": [
  #          {
  #              "content": "Step 1",
  #              "expected": "Expected Result 1"
  #          },
  #          {
  #              "content": "Step 2",
  #              "expected": "Expected Result 2"
  #         }
  #      ],
  #      "estimate": "1m 5s",
  #      "estimate_forecast": null,
  #      "id": 1,
  #      "milestone_id": 7,
  #      "priority_id": 2,
  #      "refs": "RF-1, RF-2",
  #      "section_id": 1,
  #      "suite_id": 1,
  #      "title": "Change document attributes (author, title, organization)",
  #      "type_id": 4,
  #      "updated_by": 1,
  #      "updated_on": 1393586511
  #    }
  # @see http://docs.gurock.com/testrail-api2/reference-cases
  def get_case(case_id)
    send_get("get_case/#{case_id}")
  end

##########################++
####get_cases###############
############################
  # Returns a list of test cases for a test suite or specific section in a test suite.
  # @param project_id [int] The id of the project that contains your tests
  # @param [Hash] opts
  # @option opts [int]            :suite_id       The ID of the test suite (optional if the project is operating in single suite mode)
  # @option opts [int]            :section_id     The ID of the section (optional)
  # @option opts [unix timestamp] :created_after  Only return test cases created after this date (as UNIX timestamp).
  # @option opts [unix timestamp] :created_before Only return test cases created before this date (as UNIX timestamp).
  # @option opts [int(list)]      :created_by     A comma-separated list of creators (user IDs) to filter by.
  # @option opts [int(list)]      :milestone_id   A comma-separated list of milestone IDs to filter by (not available if the milestone field is disabled for the project).
  # @option opts [int(list)]      :priority_id    A comma-separated list of priority IDs to filter by.
  # @option opts [int(list)]      :template_id    A comma-separated list of template IDs to filter by (requires TestRail 5.2 or later)
  # @option opts [int(list)]      :type_id        A comma-separated list of case type IDs to filter by.
  # @option opts [unix timestamp] :updated_after  Only return test cases updated after this date (as UNIX timestamp).
  # @option opts [unix timestamp] :updated_before Only return test cases updated before this date (as UNIX timestamp).
  # @option opts [int(list)]      :updated_by     A comma-separated list of users who updated test cases to filter by.
  # @example Code Example
  #   @client.get_cases(1, {"suite_id":1, "section_id":1})
  # @example Endpoint Example
  #   GET index.php?/api/v2/get_cases/1&suite_id=1&section_id=1
  # @example Response Example
  #   [
  #    { "id": 1, "title": "..", .. },
  #    { "id": 2, "title": "..", .. },
  #    ..
  #   ]
  # @see http://docs.gurock.com/testrail-api2/reference-cases
  def get_cases(project_id, opts = {})
    options = param_stringify(opts)
    send_get("get_cases/#{project_id.to_s}&#{options}")
  end

############################
####add_case################
############################
  # Creates a new test case
  # @param section_id [int] The ID of the section the test case should be added to
  # @param [Hash] opts
  # @option opts [string]   :title          The title of the test case (required)
  # @option opts [int]      :template_id    The ID of the template (field layout) (requires TestRail 5.2 or later)
  # @option opts [int]      :type_id        The ID of the case type
  # @option opts [int]      :priority_id    The ID of the case priority
  # @option opts [timespan] :estimate       The estimate, e.g. "30s" or "1m 45s"
  # @option opts [int]      :milestone_id   The ID of the milestone to link to the test case
  # @option opts [string]   :refs           A comma-separated list of references/requirements
  # @option opts [varies]   :custom_fields  Custom fields are supported as well and must be submitted with their system name, prefixed with 'custom_'
  # @example Code Example
  #   @client.add_case(1, {"title":"testCaseName", "type_id":1})
  # @example Endpoint Example
  #   index.php?/api/v2/add_case/1&title="foo"&type_id=1
  # @see http://docs.gurock.com/testrail-api2/reference-cases
  # @note For more information about custom fields, see guroc docs
  def add_case(section_id, opts = {})
    send_post("add_case/#{section_id.to_s}", opts)
  end

############################
####update_case#############
############################
  # Update test result by case id
  # @param case_id [int] The id of the test case
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/update_case/:case_id
  # @see http://docs.gurock.com/testrail-api2/reference-cases
  def update_case(case_id, opts = {})
    send_post("update_case/#{case_id.to_s}", opts)
  end

############################
####delete_case#############
############################
  # Delete test case by case id
  # @param case_id [int] The id of the test case
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/delete_case/:case_id
  # @see http://docs.gurock.com/testrail-api2/reference-cases
  def delete_case(case_id, opts = {})
    send_post("delete_case/#{case_id.to_s}", opts)
  end

##########################++
####get_suite###############
############################
  # Return suite by suite id
  # @param suite_id [int] The suite id
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/get_suite/:suite_id
  # @see http://docs.gurock.com/testrail-api2/reference-suites
  def get_suite(suite_id)
    send_get("get_suite/#{suite_id.to_s}")
  end

##########################++
####get_suites##############
############################
  # Return all suites in project by project id
  # @param project_id (see get_cases)
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/get_suites/:project_id
  # @see http://docs.gurock.com/testrail-api2/reference-suites
  def get_suites(project_id)
    send_get("get_suites/#{project_id.to_s}")
  end

############################
####add_suite###############
############################
  # Add a test suite
  # @param project_id [int] The id of the project containing suites
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/add_suite/:project_id
  # @see http://docs.gurock.com/testrail-api2/reference-suites
  def add_suite(project_id, opts = {})
    send_post("add_suite/#{project_id.to_s}", opts)
  end

############################
####update_suite############
############################
  # update a test suite
  # @param suite_id [int] The suite id
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/update_suite/:suite_id
  # @see http://docs.gurock.com/testrail-api2/reference-suites
  def update_suite(suite_id, opts = {})
    send_post("update_suite/#{suite_id.to_s}", opts)
  end

############################
####delete_suite############
############################
  # Delete a test suite
  # @param suite_id [int] The suite id
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/delete_suite/:suite_id
  # @see http://docs.gurock.com/testrail-api2/reference-suites
  def delete_suite(suite_id, opts = {})
    send_post("delete_suite/#{suite_id.to_s}", opts)
  end


##########################++
####get_section#############
############################
  # Return section by id
  # @param section_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/get_section/:section_id
  # @see http://docs.gurock.com/testrail-api2/reference-sections
  def get_section(section_id)
    send_get("get_section/#{section_id.to_s}")
  end

##########################++
####get_sections############
############################
  # Get sections for suite
  # @param project_id [int]
  # @param [Hash] opts
  # @options opts [int] :suite_id
  # @example Endpoint Example
  #   index.php?/api/v2/get_sections/1&suite_id=2
  # @example Code Example
  #   client.get_sections(1, {"suite_id":2})
  # @see http://docs.gurock.com/testrail-api2/reference-sections
  def get_sections(project_id, opts = {})
    options = param_stringify(opts)
    send_get("get_sections/#{project_id.to_s}&#{options}")
  end

############################
####add_section#############
############################
  # Add section to suite
  # @param project_id [int]
  # @param [Hash] opts
  # @option opts [Int] suite_id
  # @option opts [String] name
  # @option opts [Int] parent_id
  # @example Endpoint Example
  #   index.php?/api/v2/add_section/:project_id
  # @example Code Example
  #   @client.add_section(1, {"suite_id": 5, "name": "This is a new section", "parent_id": 10})
  # @see http://docs.gurock.com/testrail-api2/reference-sections
  def add_section(project_id, opts = {})
    send_post("add_section/#{project_id.to_s}", opts)
  end

############################
####get_milestone###########
############################
  # Get milestone by milestone id
  # @param milestone_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/get_milestone/:milestone_id
  # @see http://docs.gurock.com/testrail-api2/reference-milestones
  def get_milestone(milestone_id, opts = {})
    send_get("get_milestone/#{milestone_id.to_s}", opts)
  end

############################
####get_milestones##########
############################
  # Get project milestones by project id
  # @param project_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/get_milestones/:project_id
  # @see http://docs.gurock.com/testrail-api2/reference-milestones
  def get_milestones(project_id, opts = {})
    send_get("get_milestones/#{project_id.to_s}", opts)
  end

############################
####add_milestone###########
############################
  # Add milestone to project id
  # @param project_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/add_milestone/:project_id
  # @see http://docs.gurock.com/testrail-api2/reference-milestones
  def add_milestone(project_id, opts = {})
    send_post("add_milestone/#{project_id.to_s}", opts)
  end

############################
####update_milestone########
############################
  # Add milestone to project id
  # @param milestone_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/update_milestone/:milestone_id
  # @see http://docs.gurock.com/testrail-api2/reference-milestones
  def update_milestone(milestone_id, opts = {})
    send_post("update_milestone/#{milestone_id.to_s}", opts)
  end

############################
####delete_milestone########
############################
  # Add milestone to project id
  # @param milestone_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/delete_milestone/:milestone_id
  # @see http://docs.gurock.com/testrail-api2/reference-milestones
  def delete_milestone(milestone_id, opts = {})
    send_post("delete_milestone/#{milestone_id.to_s}", opts)
  end

##########################++
####get_plan################
############################
  # Get plan by id
  # @param plan_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/get_plan/:plan_id
  # @see http://docs.gurock.com/testrail-api2/reference-plans
  def get_plan(plan_id)
    send_get("get_plan/#{plan_id.to_s}")
  end

##########################++
####get_plans###############
############################
  # Get plans in project by project id
  # @param project_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/get_plans/:project_id
  # @see http://docs.gurock.com/testrail-api2/reference-plans
  def get_plans(project_id)
    send_get("get_plans/#{project_id.to_s}")
  end

############################
####add_plan################
############################
  # Add plan to project by project id
  # @param project_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/add_plan/:project_id
  # @see http://docs.gurock.com/testrail-api2/reference-plans
  def add_plan(project_id, opts = {})
    send_post("add_plan/#{project_id.to_s}", opts)
  end

############################
####add_plan_entry##########
############################
  # Add plan entries by plan id
  # @param plan_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/add_plan_entry/:plan_id
  # @see http://docs.gurock.com/testrail-api2/reference-plans
  def add_plan_entry(plan_id, opts = {})
    send_post("add_plan_entry/#{plan_id.to_s}", opts)
  end

############################
####update_plan#############
############################
  # Update plan by plan id
  # @param plan_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/update_plan/:plan_id
  # @see http://docs.gurock.com/testrail-api2/reference-plans
  def update_plan(plan_id, opts = {})
    send_post("update_plan/#{plan_id.to_s}", opts)
  end

############################
####update_plan_entry#######
############################
  # Update plan entry by plan id
  # @param plan_id [int]
  # @param entry_id [int] Id of entry
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/update_plan_entry/:plan_id/:entry_id
  # @see http://docs.gurock.com/testrail-api2/reference-plans
  def update_plan_entry(plan_id, entry_id, opts = {})
    send_post("update_plan_entry/#{plan_id.to_s}/#{entry_id.to_s}", opts)
  end

############################
####close_plan##############
############################
  # Close plan by plan id
  # @param plan_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/close_plan/:plan_id
  # @see http://docs.gurock.com/testrail-api2/reference-plans
  def close_plan(plan_id, opts = {})
    send_post("close_plan/#{plan_id.to_s}", opts)
  end

############################
####delete_plan#############
############################
  # @example Endpoint Example
  #   index.php?/api/v2/delete_plan/:plan_id
  # @see http://docs.gurock.com/testrail-api2/reference-plans
  def delete_plan(plan_id, opts)
    send_post("delete_plan/#{plan_id.to_s}", opts)
  end

############################
####delete_plan_by_entry####
############################
  # @example Endpoint Example
  #   index.php?/api/v2/delete_plan_entry/:plan_id/:entry_id
  def delete_plan_entry(plan_id, entry_id, opts = {})
    send_post("delete_plan_entry/#{plan_id.to_s}/#{entry_id.to_s}", opts)
  end

##########################++
####get_project#############
############################
  # Get project by project id
  # @param project_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/get_project/:project_id
  # @see http://docs.gurock.com/testrail-api2/reference-projects
  def get_project(project_id, opts = {})
    options = param_stringify(opts)
    send_get("get_project/#{project_id.to_s}&#{options}")
  end

##########################++
####get_projects############
############################
  # Get all projects
  # @param [Hash] opts
  # @option opts [bool] :is_completed 1 == completed projects, 2 == active projects
  # @example Endpoint Example
  #   index.php?/api/v2/get_projects
  # @example Code Example [get all projects]
  #   client.get_projects
  # @example Code Example [get active projects]
  #   client.get_projects({'is_completed':0})
  # @example Code Example [get completed projects]
  #   client.get_projects({'is_completed':1})
  # @example Response Example
  #   [
  #    { "id": 1, "name": "Project1", ... },
  #    { "id": 2, "name": "Project2", ... },
  #    ..
  #   ]
  # @see http://docs.gurock.com/testrail-api2/reference-projects
  def get_projects(opts = {})
    options = param_stringify(opts)
    send_get("get_projects&#{options}")
  end

############################
####add_project#############
############################
  # Add a project
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/add_project
  # @see http://docs.gurock.com/testrail-api2/reference-projects
  def add_project(opts = {})
    send_post("add_project", opts)
  end

############################
####update_project##########
############################
  # Update a project
  # @param project_id [int] The project you want to update
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/update_project/:project_id
  # @see http://docs.gurock.com/testrail-api2/reference-projects
  def update_project(project_id, opts)
    send_post("update_project/#{project_id.to_s}", opts)
  end

############################
####delete_project##########
############################
  # Delete a project
  # @param project_id [int] The project you want to delete
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/delete_project/:project_id
  # @see http://docs.gurock.com/testrail-api2/reference-projects
  def delete_project(project_id, opts)
    send_post("delete_project/#{project_id.to_s}", opts)
  end

##########################==
####get_results#############
############################
  # Returns a list of test results for a test
  # @param test_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/get_results/:test_id
  # @see http://docs.gurock.com/testrail-api2/reference-results
  def get_results(test_id, opts = {})
    options = param_stringify(opts)
    send_get("get_results/#{test_id.to_s}&#{options}")
  end

##########################==
####get_results_for_case####
############################
  # Returns a list of test results for a test run and case combination
  # @param run_id [int]
  # @param case_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/get_results_for_case/:run_id/:case_id
  # @see http://docs.gurock.com/testrail-api2/reference-results
  def get_results_for_case(run_id, case_id, opts = {})
    options = param_stringify(opts)
    send_get("get_results_for_case/#{run_id.to_s}/#{case_id.to_s}&#{options}")
  end

##########################==
####get_results_for_run#####
############################
  # Returns a list of test results for a test run
  # @param run_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/get_results_for_run/:run_id
  # @see http://docs.gurock.com/testrail-api2/reference-results
  def get_results_for_run(run_id, opts = {})
    options = param_stringify(opts)
    send_get("get_results_for_run/#{run_id.to_s}&#{options}")
  end

############################
####add_result##############
############################
  # Adds a new test result, comment or assigns a test. It's recommended to use add_results instead if you plan to add results for multiple tests.
  # @param test_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/add_result/:test_id
  # @see http://docs.gurock.com/testrail-api2/reference-results
  def add_result(test_id, opts = {})
    send_post("add_result/#{test_id.to_s}", opts)
  end

############################
####add_result_for_case#####
############################
  # Adds a new test result, comment or assigns a test (for a test run and case combination)
  # @param run_id [int]
  # @param case_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/add_result_for_case/:run_id/:case_id
  # @see http://docs.gurock.com/testrail-api2/reference-results
  def add_result_for_case(run_id, case_id, opts = {})
    send_post("add_result_for_case/#{run_id.to_s}/#{case_id.to_s}", opts)
  end

############################
####add_results#############
############################
  # Adds one or more new test results, comments or assigns one or more tests
  # @param run_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/add_results/:run_id
  # @see http://docs.gurock.com/testrail-api2/reference-results
  def add_results(run_id, opts = {})
    send_post("add_results/#{run_id.to_s}", opts)
  end

############################
####add_results_for_cases###
############################
  # Adds one or more new test results, comments or assigns one or more tests (using the case IDs)
  # @param run_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/add_results_for_cases/:run_id
  # @see http://docs.gurock.com/testrail-api2/reference-results
  def add_results_for_cases(run_id, opts = {})
    send_post("add_results_for_cases/#{run_id.to_s}", opts)
  end

##########################==
####get_run#################
############################
  # Get run by run id
  # @param run_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/get_run/:run_id
  # @see http://docs.gurock.com/testrail-api2/reference-runs
  def get_run(run_id)
    send_get("get_run/#{run_id.to_s}")
  end

##########################==
####get_runs################
############################
  # Get runs by project id
  # @param project_id [int]
  # @param plan_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/get_runs/:project_id
  # @see http://docs.gurock.com/testrail-api2/reference-runs
  def get_runs(project_id, opts = {})
    options = param_stringify(opts)
    send_get("get_runs/#{project_id.to_s}&#{options}")
  end

############################
####add_run#################
############################
  # Add run by suite id
  # @param project_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/add_run/:project_id
  # @see http://docs.gurock.com/testrail-api2/reference-runs
  def add_run(project_id, opts = {})
    send_post("add_run/#{project_id.to_s}", opts)
  end

############################
####update_run##############
############################
  # Updates existing test run
  # @param run_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/update_run/:run_id
  # @see http://docs.gurock.com/testrail-api2/reference-runs
  def update_run(run_id, opts = {})
    send_post("update_run/#{run_id.to_s}", opts)
  end

############################
####close_run###############
############################
  # Closes an existing test run and archives its tests & results
  # @param run_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/close_run/:run_id
  # @see http://docs.gurock.com/testrail-api2/reference-runs
  def close_run(run_id, opts = {})
    send_post("close_run/#{run_id.to_s}", opts)
  end

############################
####delete_run##############
############################
  # Deletes an existing test run.
  # @param run_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/delete_run/:run_id
  # @see http://docs.gurock.com/testrail-api2/reference-runs
  def delete_run(run_id, opts = {})
    send_post("delete_run/#{run_id.to_s}", opts)
  end

##########################==
####get_test################
############################
  # Returns an existing test
  # @param test_id [int]
  # @example Endpoint Example
  #   index.php?/api/v2/get_test/:test_id
  # @see http://docs.gurock.com/testrail-api2/reference-tests
  def get_test(test_id)
    send_get("get_test/#{test_id.to_s}")
  end

##########################==
####get_tests###############
############################
  # Returns a list of tests for a test run
  # @param run_id [int]
  # @param [Hash] opts
  # @example Endpoint Example
  #   index.php?/api/v2/get_tests/:run_id
  # @see http://docs.gurock.com/testrail-api2/reference-tests
  def get_tests(run_id, opts = {})
    options = param_stringify(opts)
    send_get("get_tests/#{run_id.to_s}&#{options}")
  end

end #close module
