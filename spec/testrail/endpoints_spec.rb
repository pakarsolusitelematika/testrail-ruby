require 'spec_helper'
require_relative '../../lib/testrail-ruby'


# Must set these to run tests
# gitlab varables, .bashrc, etc

base_url = ENV["TESTRAIL_BASE_URL"]
user     = ENV["TESTRAIL_USER"]
password = ENV["TESTRAIL_PASSWORD"]

client = TestRail::APIClient.new(base_url)
client.user = user
client.password = password

RSpec.describe "get_case" do
  response  = client.get_case(1462)
  it 'is not empty' do
    puts "Response:\n#{response}"
    expect(response).not_to be_empty
  end
end #describe

RSpec.describe "get_cases" do
  response  = client.get_cases(1, {"suite_id":1, "section_id":1})
  it 'is not empty' do
    puts "Response:\n#{response}"
    expect(response).not_to be_empty
  end
end

RSpec.describe "add_case" do
  response  = client.add_case(1, {"title":"testCaseName", "type_id":1})
  it 'is not empty' do
    puts "Response:\n#{response}"
    expect(response).not_to be_empty
  end
end

RSpec.describe "get_section" do
  response  = client.get_section(1)
  it 'is not empty' do
    puts "Response:\n#{response}"
    expect(response).not_to be_empty
  end
end

RSpec.describe "get_sections" do
  response  = client.get_sections(1, {"suite_id":1, "section_id":1})
  it 'is not empty' do
    puts "Response:\n#{response}"
    expect(response).not_to be_empty
  end
end

RSpec.describe "get_suite" do
  response  = client.get_suite(1)
  it 'is not empty' do
    puts "Response:\n#{response}"
    expect(response).not_to be_empty
  end
end

RSpec.describe "get_suites" do
  response  = client.get_suites(1)
  it 'is not empty' do
    puts "Response:\n#{response}"
    expect(response).not_to be_empty
  end
end

RSpec.describe "get_plan" do
  response  = client.get_plan(21)
  it 'is not empty' do
    puts "Response:\n#{response}"
    expect(response).not_to be_empty
  end
end

RSpec.describe "get_project" do
  response  = client.get_project(1)
  it 'is not empty' do
    puts "Response:\n#{response}"
    expect(response).not_to be_empty
  end
end

RSpec.describe "get_projects" do

  # it 'is_complete 1 (true) is not empty' do
  #   response  = client.get_projects({"is_completed":1})
  #   puts "Response:\n#{response}"
  #   expect(response).not_to be_empty
  # end

  it 'is_complete(false) is not empty' do
    response  = client.get_projects({"is_completed":0})
    puts "Response:\n#{response}"
    expect(response).not_to be_empty
  end
end
